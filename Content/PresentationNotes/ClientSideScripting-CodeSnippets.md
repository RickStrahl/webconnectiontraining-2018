﻿```javascript
$("#myID").css("color","steelblue");

$(".myClass1,.myclass2").hide();

$("table tbody tr:even")
         .addClass("gAlternate")
         .css( {fontWeight:"bold",padding:"3px"} );

$("#gdEntries tr")
	.click(function(e){ alert( $(e.target).text() );});
	
$("<div>").attr("id","divNew")
          .appendTo(document.body);

$.get("https://albumviewer.west-wind.com/api/artists",
      function(result) { alert(result[0].ArtistName); })         
}) ;        
```


```js
// Create a model
var vm = {}
vm = {
    projects: [],
    newProject: {
        projname: "",
        startdate: moment(new Date()).format("M/D/Y"),
        status: 0,
        pk: 0,
        customerpk: custId
    },
    ready: false,
    addRowVisible: false,
    deleteProject: function(project) {
    	ajaxJson(baseUrl + 
           "deleteProject.ttr?id="+ project.pk,null,
            function success(result) {...},
            function error() {...});
    }
}

// Bind the Model to Vue and a DOM element
var app = new Vue({
    el: "#CustomerPage",
    data: function () {
        return vm;
    }
});

```

```html
<div id="#CustomerPage">
<div  class="list" v-bind:class="{'hidden': !ready }">
    <div class="list-header">
        <button type="button" id="btnAdd"
                v-on:click="addRowVisible = !addRowVisible">
            Add
        </button>
        Recent Projects
    </div>
    <div class="list-item" v-bind:class="{'hidden': !addRowVisible }">                
            <div class="responsive-container">

                <input type="text" class="form-control" placeholder="New project name"
                       v-model="newProject.projname"
                       v-validate="'required|min:3'"
                       name="ProjectName" id="ProjectName" />
                <div style="width: 250px;">
                    <%= HtmlDateTextBox("StartDate","",[ placeholder="starts on"   
                                        v-model="newProject.startdate"]) %>
                </div>
                <button type="button"
                        class="btn btn-primary"
                        @click="saveProject()"
                        v-bind:disabled="errors.any() || !newProject.projname">
                    Save
                </button>
            </div>

            <div class="text-danger" style="margin-top: 6px;" v-show="errors.any()">
                {{errors.first('ProjectName')}} {{errors.first('StarDate')}}
            </div>
    </div>

    <div v-for="project in projects"
         class="list-item">
        <a v-on:click="removeProject(project)"
           class="float-right btn">
            <i class="fas fa-times-circle text-danger"></i>
        </a>
        <div class="list-item-header ">
            <i class="float-left fa-2x fa-fw far project-icon "
               v-bind:class="{
                'fa-clipboard-check': project.status == 1,
                'fa-arrow-alt-circle-down': project.status ==0 }"></i>
            <a v-bind:href="'project.ttk?id=' + project.pk">{{project.projname}}</a>
        </div>
        <div class="small font-italic">{{project.entered}}</div>
    </div>
</div>
</div>
```
