﻿```foxpro
FUNCTION  Entry()

PRIVATE poError
poError = CREATEOBJECT("HtmlErrorDisplayConfig")

lnId = VAL(Request.Params("id"))
IF lnId = 0
   Response.Redirect("~/EntryBrowser.ttk")
ENDIF

loEntry = CREATEOBJECT("ttEntry")

*** doesn't exist - leave
if(!loEntry.Load(lnid))
   Response.Redirect("~/EntryBrowser.ttk")
ENDIF

*** Create the Model for the view
PRIVATE poEntry
poEntry = loEntry.oData

IF !poEntry.PunchedOut
   Response.Redirect(FormatString("~/punchout.ttk?id={0}",poEntry.Pk))
   RETURN   
ENDIF

*** Render the View
Response.ExpandScript()
ENDFUNC
```


```html
<% Layout="~/views/_layoutpage.wcs" %>

    <%= HtmlErrorDisplay(poError) %>

<div class="row">
	<div class="form-group">
	    <label for="Title" class="control-label hidable">Title:</label>
	    <div class="entry-title">
	        <%= poEntry.title %>
	    </div>
	</div>
	
	<div class="form-group">
	    <label class="control-label hidable" for="customerpk">Company:</label>
	    <div>
	        <a href="~/customer.ttk?id=<%= poEntry.CustomerPk %>">
	            <%= poEntry.Customer.Company %>
	        </a>
	    </div>
	</div>
	
	<div class="form-group">
	    <label class="control-label hidable" for="projectpk">Project:</label>
	    <div>
	        <a href="project.ttk?id=<%= poEntry.projectPk %>">
	            <%= poEntry.Project.ProjName %>
	        </a>
	    </div>
	</div>
	
	<hr />
	<div>
	    <%= Markdown(poEntry.descript)  %>
	</div>
</div>
```

```html
<% section="scripts" %>
<script src="~/lib/jquery/dist/jquery.min.js"></script>
<% endsection %>
```
