﻿```foxpro
do wwJsonSerializer

loObject = CREATEOBJECT("Empty")
ADDPROPERTY(loObject,"Name","Rick")
ADDPROPERTY(loObject,"Company","West Wind")
ADDPROPERTY(loObject,"Entered",DATETIME())

loSer = CREATEOBJECT("wwJsonSerializer")

*** Produces: {"company":"West Wind","entered":"2012-02-27T23:31:49Z","name":"Rick"}
*** Properties are always rendered lower case
? loSer.Serialize(loObject)

*** Make sure these properties render in proper case
loSer.PropertyNameOverrides = "Name,Company,Entered"

*** Produces: {"Company":"West Wind","Entered":"2012-02-27T23:31:49Z","Name":"Rick"}
*** specified properties are overridden
? loSer.Serialize(loObject)
```

```foxpro
do wwJsonSerializer
loSer = CREATEOBJECT("wwJsonSerializer")
lcJson = loSer.Serialize(loObject)  && Objects, Values, Collections
? lcJson  

*** You can serialize Cursors
select * from customers into TCustomers
lcJson = loSer.Serialize("cursor:TCustomers")  && Tables/Cursors

*** To deserialize JSON
loObject = loSer.DeserializeJson(lcJson)  && JSON Objects, Values, Arrays 
? loObject.Value
```




```foxpro
****************************************
***  Function: Generic endpoint for callbacks
***    Assume: Callbacks.ttk?method=UpdateEntryTotals
***            POST JSON to be passed as input parameter (if any)
************************************************************************
FUNCTION Callbacks()

*** Instantiate or reference the class that will will handle the method call
*** Here I just use the Process class itself which will lhandle the call
loTarget= this  && Class that handles callback

*** Instanantiate the service
loService = CREATEOBJECT("wwJsonService")

*** Return a JSON Response
Response.ContentType = "application/json"

*** Write out the result from CallMethod which returns the JSON for the method
*** or a JSON Error object if the call failed
lcResult = loService.CallMethod(Request,loTarget) 

*** Return an error response so jQuery's error handler kicks in
*** Note: Result is still a JSON response
IF loService.ErrorOccurred
   Response.Status = "500 Server Error"
ENDIF

Response.Write(lcResult)
RETURN
```

```foxpro
************************************************************************
*  CustomersRecentEntries
****************************************
FUNCTION CustomersRecentEntries(loParm)

lcFilter = ""
pcSearchFor = loParm.Search
lnCount = loParm.maxCount

IF !EMPTY(pcSearchFor)
	pcSearchFor = "%" + LOWER(pcSearchFor) + "%"
    lcFilter = "lower(Company) like ?pcSearchFor OR LOWER(lastname) like ?pcSearchFor"
ENDIF

loCustomer = CREATEOBJECT("ttCustomer")
loCustomerList = loCustomer.GetCustomerListWithRecentOrders(;
					"pk,Company,Name ,LastOrder as LastAccess",;
					lcFilter, lnCount)

RETURN loCustomerList
```


```foxpro
loHttp = CREATEOBJECT("wwHttp")
lcAlbumsJson = loHttp.HttpGet("https://albumviewer.west-wind.com/api/albums")

loSer = CREATEOBJECT("wwJsonSerializer")
loAlbums = loSer.DeSerializeJson(lcAlbumsJson)

FOR EACH loAlbum IN loAlbums FOXOBJECT
   ? loAlbum.Title + " - "  + loAlbum.Artist.ArtistName + " (" + TRANSFORM(loAlbum.Year) + ")"
   IF !ISNULL(loAlbum.Tracks)
	   FOR EACH loTrack IN loAlbum.Tracks FOXOBJECT
		   ? "  " + loTrack.SongName + " "  + NVL(loTrack.Length,"")
	   ENDFOR
	ENDIF
ENDFOR
```


```foxpro
DO wwJsonSerializer
loClient = CREATEOBJECT("wwJsonServiceClient")

loArtist = loClient.CallService("https://albumviewer.west-wind.com/api/artist", ;
                                 loArtistData, "POST")

IF  loClient.lError
   ? loClient.cErrorMsg
   RETURN
ENDIF

*** Artist object returned
? loArtist.Id
```


```foxpro
DO AlbumServiceClient

LOCAL loClient as AlbumServiceClient
loClient = CREATEOBJECT("AlbumServiceClient")

loArtists = loClient.GetArtists()
IF ISNULL(loAlbums)
    ? "Couldn't retrieve artists: " + loClient.cErrorMsg
ENDIF   

*** Retrieve Anthrax artist
loResult = loClient.GetArtist(6)
loArtist = loResult.Artist  && API returns artist and albums 

IF !loClient.Login("test","test")
   ? "Couldn't log in to update artist..."
ENDIF

*** Update artist and send it back
loArtist.ArtistName = "Anthrax - " + TIME()
loResult = loCLient.UpdateArtist(loArtist)

IF ISNULL(loResult)
   ? "Couldn't update artist: " + loClient.cErrorMsg
ENDIF

? loResult.Artist.ArtistName
```

```foxpro
*** Add CORS header to allow cross-site access from other domains/mobile devices on Ajax calls
 Response.AppendHeader("Access-Control-Allow-Origin","*")
 Response.AppendHeader("Access-Control-Allow-Methods","POST, GET, DELETE, PUT, OPTIONS")
 Response.AppendHeader("Access-Control-Allow-Headers","Content-Type, *")
 *** Allow cookies and auth headers
 Response.AppendHeader("Access-Control-Allow-Credentials","true")
 
 *** CORS headers are requested with OPTION by XHR clients. OPTIONS returns no content
lcVerb = Request.GetHttpVerb()
IF (lcVerb == "OPTIONS")
   *** Just exit with CORS headers set
   *** Required to make CORS work from Mobile devices
   RETURN .F.
ENDIF
```

