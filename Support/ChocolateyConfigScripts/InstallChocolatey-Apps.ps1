choco feature enable -n allowGlobalConfirmation  

# Apps
choco install google-chrome -y
choco install Firefox -y
choco install filezilla -y
choco install vscode -y
choco install markdownmonster -y

choco install skype -y
choco install zoom -y
choco install dropbox -y 

choco install paint.net -y
choco install teamviewer -y 
choco install onenote -y
choco install iTunes -y
choco install speccy -y 
choco install picasa -y 
choco install Recuva -y
choco install snagit -y
choco install camtasia -y

# System
choco install curl -y
choco install windirstat -y
choco install 7zip.install -y



