# Better to run this command on its own then restart the console
# so paths are set
# Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

choco feature enable -n allowGlobalConfirmation
choco install -y disableuac

# Apps
choco install -y GoogleChrome
choco install -y vscode

choco install -y 7zip.install
choco install -y curl

choco install -y filezilla
choco install -y git
choco install -y tortoisegit
# choco install -y SourceTree
choco install -y xplorer2
choco install -y procexp
choco install -y deletefiles
choco install -y markdownmonster
#choco install -y westwindwebsurge

# choco install -y filezilla.server

# choco install mssql2016express-defaultinstance
# choco install mssqlservermanagementstudio2014express