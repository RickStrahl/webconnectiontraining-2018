************************************************************************
*PROCEDURE UserSecurityManagerProcess
****************************
***  Function: Processes incoming Web Requests for UserSecurityManagerProcess
***            requests. This function is called from the wwServer 
***            process.
***      Pass: loServer -   wwServer object reference
*************************************************************************
LPARAMETER loServer
LOCAL loProcess
PRIVATE Request, Response, Server, Session, Process
STORE NULL TO Request, Response, Server, Session, Process


#INCLUDE WCONNECT.H

loProcess = CREATEOBJECT("UserSecurityManagerProcess", loServer)
loProcess.lShowRequestData = loServer.lShowRequestData

IF VARTYPE(loProcess)#"O"
   *** All we can do is return...
   RETURN .F.
ENDIF

*** Call the Process Method that handles the request
loProcess.Process()

*** Explicitly force process class to release
loProcess.Dispose()

RETURN

*************************************************************
DEFINE CLASS UserSecurityManagerProcess AS WWC_PROCESS
*************************************************************

*** Response class used - override as needed
cResponseClass = [WWC_PAGERESPONSE]

*** Default for page script processing if no method exists
*** 1 - MVC Template (ExpandTemplate()) 
*** 2 - Web Control Framework Pages
*** 3 - MVC Script (ExpandScript())
nPageScriptMode = 3

cAuthenticationMode = "UserSecurity"  && `Basic` is default
cAuthenticationUserSecurityClass = "AppUserSecurity"

*** Configuration Settings
cBaseUrl = "~/usermanager/"     && "~/usermanager/" 

 
*********************************************************************
* Function UserSecurityManagerProcess :: OnProcessInit
************************************
*** If you need to hook up generic functionality that occurs on
*** every hit against this process class , implement this method.
*********************************************************************
FUNCTION OnProcessInit

LOCAL lcScriptName, llForceLogin
THIS.InitSession(this.oConfig.cCookieName,3600,.t.)

lcScriptName = LOWER(JUSTFNAME(Request.GetPhysicalPath()))

*** Force login to all requests EXCEPT the ones in the list
llIgnoreLoginRequest =  INLIST(lcScriptName,;
  "default","login","logout","profile",;
  "validateemail","emailpassword","recoverpassword")

IF !THIS.Authenticate("any","",llIgnoreLoginRequest) 
   IF !llIgnoreLoginRequest
	  RETURN .F.
   ENDIF
ENDIF

*** Explicitly specify that pages should encode to UTF-8 
*** Assume all form and query request data is UTF-8
Response.Encoding = "UTF8"
Request.lUtf8Encoding = .T.


*** Add CORS header to allow cross-site access from other domains/mobile devices on Ajax calls
*!* Response.AppendHeader("Access-Control-Allow-Origin","*")
*!* Response.AppendHeader("Access-Control-Allow-Origin",Request.ServerVariables("HTTP_ORIGIN"))
*!* Response.AppendHeader("Access-Control-Allow-Methods","POST, GET, DELETE, PUT, OPTIONS")
*!* Response.AppendHeader("Access-Control-Allow-Headers","Content-Type, *")
*!* *** Allow cookies and auth headers
*!* Response.AppendHeader("Access-Control-Allow-Credentials","true")
*!* 
*!* *** CORS headers are requested with OPTION by XHR clients. OPTIONS returns no content
*!*	lcVerb = Request.GetHttpVerb()
*!*	IF (lcVerb == "OPTIONS")
*!*	   *** Just exit with CORS headers set
*!*	   *** Required to make CORS work from Mobile devices
*!*	   RETURN .F.
*!*	ENDIF   


RETURN .T.
ENDFUNC


************************************************************************
*  Login
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION Login()
PRIVATE pcErrorMessage, pcDisplayMesage, pcReturnUrl

pcDisplayMessage = Request.Params("message")
IF EMPTY(pcDisplayMessage)
	pcDisplayMessage = "" 
	* [No account? <a href="~/usermanagers/Profile.usm">Click here to create one</a>.]
ENDIF	
pcErrorMessage = ""

pcReturnUrl = Request.Params("ReturnUrl")
IF EMPTY(pcReturnUrl)
   pcReturnUrl = "~/"
ENDIF

IF Request.IsPostback()
    IF Request.IsFormVar("WebLogin_btnRecover")
		THIS.EmailPassword()
		pcErrorMessage = "Please check your email for an email with a link to recover your password."	
	ELSE
	   IF this.Authenticate("ANY",@pcErrorMessage)	   
	      Response.Redirect(pcReturnUrl) && if a return URL was provided use it
	   ENDIF
        *** Authenticate renders the login form if we failed
	   RETURN
   ENDIF   
ENDIF

*** GET request - just show the form
this.OnShowAuthenticationForm()
ENDFUNC
*   Login

************************************************************************
*  Logout
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION Logout()
*** Force authentication to log out
THIS.Authenticate("LOGOUT")
THIS.oUser = null
Response.Redirect("~/")
ENDFUNC
* Logout



************************************************************************
*  Profile
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION Profile()

#IF .F. 
LOCAL Request as wwRequest, Response as wwPageResponse, Server as wwServer, ;
      THIS as wwProcess, Process as wwProcess, Session as wwSession
#ENDIF


*** Error and binding info goes here and displayed in <%= HtmlErrorDisplay(poError) %>
poError = CREATEOBJECT("HtmlErrorDisplayConfig")

poModel = CREATEOBJECT("EMPTY")
ADDPROPERTY(poModel,"lIsNewAccount",.T.)

IF THIS.lIsAuthenticated
   IF TYPE("this.oUserSecurity.oUser") = "O"
	   poUser = this.oUserSecurity.oUser
	   poModel.lIsNewAccount = .F.
   ELSE
   	   this.oUserSecurity = CREATEOBJECT(this.cAuthenticationUserSecurityClass)
       poUser = this.oUserSecurity.GetUserByUsername(this.cAuthenticatedUser)
       IF VARTYPE(poUser) # "O"
			this.oUserSecurity.NewUser()
			poUser = this.oUserSecurity.oUser
       ELSE
		  poModel.lIsNewAccount = .F.
       ENDIF
       
   ENDIF
ELSE
	this.oUserSecurity = CREATEOBJECT(this.cAuthenticationUserSecurityClass)
	this.oUserSecurity.NewUser()
	poUser = this.oUserSecurity.oUser
ENDIF


IF (Request.IsPostBack())
   poError.Errors = Request.UnbindFormVars(poUser,.f.,"pk,password,active,admin")
     

   *** Password logic - if not empty &&
   pcPassword = Request.Form("Password")
   pcPasswordConfirm = Request.Form("PasswordConfirm")
   IF !EMPTY(pcPassword) 
   	  IF pcPassword != pcPasswordConfirm
	     poError.Errors.AddError("Passwords entered don't match.","Password")
      ELSE
    	 poUser.Password = pcPassword
	  ENDIF
   ENDIF
  
   IF poError.Errors.Count < 1 
       IF !this.oUserSecurity.SaveUser()
          poError.Errors.AddError(this.oUserSecurity.cErrorMsg)
       ENDIF  
   ENDIF

   IF (poError.Errors.Count > 0)
       poError.Message = poError.Errors.ToHtml()
   	   poError.Header = "Please fix the following form entry errors"
   ELSE

        *** Account Validation via Email confirmation
    	IF this.oUserSecurity.lRequireValidation AND ;
    	   poModel.lIsNewAccount 
			lcMessage =  ;
			  	"Please click on the link below to verify your Email address:"  +CRLF + CRLF +;
			  	THIS.ResolveServerUrl("~/ValidateEmail.usm?id=" + poUser.Validate) + CRLF + CRLF + ;
			  	"Click on this link or paste it into your browser to visit this page, and then sign in to your account." + CRLF + CRLF+ CRLF + CRLF +;
			  	"The " + this.oConfig.cCompanyName + " Team"			  	
			  	
  		   lcError = ""

		   IF THIS.SendAdminEmail("Email Account Email Verification",;
		      lcMessage,;
	    	      poUser.Username,;
	    	      this.oConfig.cEmailSenderName + ;
	    	      "<" +  this.oConfig.cEmailSender + ">",.F.,.F.,@lcError)
    	     
			   Response.Redirect("login.usm?message=" + ;
				   "An email confirmation has been sent to you to validate your email address. "+;
				   "Please click on the link in the email to validate your email.")
			   RETURN
		   ENDIF
		   
		   *** Failed email - set it active - otherwise account is locked out
		   poUser.Active = .T.
		   this.oUserSecurity.SaveUser()  && save again		   
		ENDIF
   
       poError.Message = "Entry saved."
       poError.Icon = "info"
       Response.AppendHeader("Refresh","2;url=" + THIS.ResolveUrl("~/"))
   ENDIF
ENDIF
 
*** Simplify model values
ADDPROPERTY(poModel,"oUser",null)

Response.ExpandScript("~/usermanager/profile.usm")
ENDFUNC
*   Profile

************************************************************************
*  EmailPassword
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION EmailPassword(lcRecipient)
LOCAL lcEmail, lcError
* pcErrorMessage is passed through

#IF .F. 
LOCAL Request as wwRequest, Response as wwPageResponse, Server as wwServer, ;
      THIS as wwProcess, Process as wwProcess, Session as wwSession
#ENDIF


lcEmail = Request.Form("WebLogin_txtUsername")
IF EMPTY(lcEmail)
   pcErrorMessage = "Please specify an email address to recover password for."
   RETURN
ENDIF
   
loUserSecurity = CREATEOBJECT(this.cAuthenticationUserSecurityClass)
IF !loUserSecurity.GetUserByUserName(lcEmail)
   pcErrorMessage = "Invalid email address."
   RETURN
ENDIF

loUserSecurity.oUser.Validate = GetUniqueId(16)
loUserSecurity.SaveUser()

lcMessage =  ;
  	"To recover your password for your account please go to this URL: "  +CRLF + CRLF +;
  	THIS.ResolveServerUrl("~/usermanager/RecoverPassword.usm?id=" + loUserSecurity.oUser.Validate) + CRLF + CRLF + ;
  	"Click on this link or paste it into your browser to visit this page, and then enter a new password." + CRLF + CRLF+ + CRLF + CRLF+;
  	"The " + this.oConfig.cCompanyName + " Team"
 
lcError= ""
 
IF !this.SendAdminEmail("Password Recovery",lcMessage,loUserSecurity.oUser.UserName,;
                        this.oConfig.cEmailSenderName + "<" +  this.oConfig.cEmailSender + ">",.F.,.F.,@lcError)
   pcErrorMessage = "Failed to send confirmation email: " + lcError
ELSE  
   pcErrorMessage = "Password recovery email has been sent."
ENDIF   	
   
ENDFUNC
*   EmailPassword

************************************************************************
*  ValidateEmail
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION ValidateEmail()
LOCAL lcPassword, lcVerify

#IF .F. 
LOCAL Request as wwRequest, Response as wwPageResponse, Server as wwServer, ;
      THIS as wwProcess, Process as wwProcess, Session as wwSession
#ENDIF

SET STEP ON

loUserSecurity = CREATEOBJECT(this.cAuthenticationUserSecurityClass)
lcValidationKey = Request.QueryString("id")


IF !loUserSecurity.GetUserByValidationKey(lcValidationKey)
   Response.Redirect("login.lk1?message=Invalid validation key")
ELSE
   loUserSecurity.oUser.Active = .T.
   loUserSecurity.oUser.Validate = ""
   loUserSecurity.SaveUser()
   THIS.Authenticate("LOGOUT")
   Response.Redirect("login.lk1?message=Your account is validated. You can access your account now by signing in.")
ENDIF

ENDFUNC
*   ValidateEmail

************************************************************************
*  RecoverPassword
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION RecoverPassword()
LOCAL lcPassword, lcVerify

poError = CREATEOBJECT("HtmlErrorDisplayConfig")
loUserSecurity = CREATEOBJECT(this.cAuthenticationUserSecurityClass)

lcValidationKey = Request.QueryString("id")

IF !loUserSecurity.GetUserByValidationKey(lcValidationKey)
   Response.Redirect("~/login.usm")  && don't screw around - invalid id
   RETURN
ENDIF

plConfirmation = .F.

IF Request.IsPostBack()
   lcPassword = Request.Form("Password")
   lcVerify = Request.Form("Password_Verify")
   
   IF (EMPTY(lcPassword) )
      poError.Message = "Password can't be empty."
   ELSE
  	  IF(lcVerify != lcPassword)
	   	  poError.Message = "Passwords entered don't match."
	  ELSE
	      loUSerSecurity.oUser.Password = lcPassword
	      loUserSecurity.oUser.Validate = ""
	      loUserSecurity.oUser.Active = .T.
	      
	      IF (!loUserSecurity.SaveUser())
	        *** Display Error Message
	      	poError.Message = "failed to save the user: " + loUserSecurity.cErrorMsg
	      ELSE
	        *** Show the login page.
	        poError.Message = "Your password has been changed. Please log in with your new credentials."	        
	        plConfirmation = .T.			
	        Response.Redirect("~/login.usm?message=" + poError.Message)
	      ENDIF
	  ENDIF
   ENDIF
ENDIF

Response.ExpandScript()
ENDFUNC
*   RecoverPassword



************************************************************************
*  UserList
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION UserList()
PRIVATE pnUserCount

#IF .F. 
LOCAL Request as wwRequest, Response as wwPageResponse, Server as wwServer, ;
      THIS as wwProcess, Process as wwProcess, Session as wwSession
#ENDIF

pnUserCount = 0

*** Admin Only
IF !this.oUserSecurity.oUser.Admin
   Response.Redirect("~/")
   RETURN
ENDIF

pnUserCount = THIS.oUserSecurity.GetUsers()

Response.ExpandScript()
ENDFUNC
*   UserList


************************************************************************
*  UserInfo
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION UserInfo()

#IF .F. 
LOCAL Request as wwRequest, Response as wwPageResponse, Server as wwServer, ;
      THIS as wwProcess, Process as wwProcess, Session as wwSession, ;
      loSecurity as wwUserSecurity
#ENDIF

*** Admin Only
IF !this.oUserSecurity.oUser.Admin
   Response.Redirect("~/")
   RETURN
ENDIF

poError = CREATEOBJECT("HtmlErrorDisplayConfig")
poModel = CREATEOBJECT("EMPTY")
ADDPROPERTY(poModel,"oUserSecurity",this.oUserSecurity)
ADDPROPERTY(poModel,"oUser",null)
ADDPROPERTY(poModel,"lIsNewUser",.F.)
ADDPROPERTY(poModel,"cId","")


poModel.cId = Request.Params("Id")

IF !poModel.oUserSecurity.GetUser(poModel.cId) 
   poModel.oUserSecurity.NewUser()
   poModel.oUser = poModel.oUserSecurity.oUser
   poModel.lIsNewUser = .T.
   
   *** We need to have a password in order to save a new user
   poModel.oUser.Password = GetUniqueId()
   poModel.oUser.Active = .T.
ENDIF

poModel.oUser = poModel.oUserSecurity.oUser

IF (Request.IsPostBack())
   poError.Errors = Request.UnbindFormVars(this.oUserSecurity.oUser,.f.,"pk")
	IF poError.Errors.Count < 1 
       IF !this.oUserSecurity.SaveUser()        
	      poError.Errors.AddError(this.oUserSecurity.cErrorMsg)    
	      IF poModel.lIsNewUser
	         poModel.oUser.pk = ""  && clear out PK so the user stays as 'new'
	      ENDIF  
       ENDIF  
   ENDIF

   IF (poError.Errors.Count > 0)
   	  poError.Message = poError.Errors.ToHtml()
   	   poError.Header = "Please fix the following form entry errors"
   ELSE
       poError.Message = "Entry saved."
       poError.Icon = "info"
       Response.AppendHeader("Refresh","2;url=" + Process.ResolveUrl("~/usermanager/admin/userlist.usm"))
   ENDIF               
ENDIF

Response.ExpandScript()
ENDFUNC
*   UserInfo

************************************************************************
*  DeleteUser
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION DeleteUser()

*** Admin Only
IF !this.oUserSecurity.oUser.Admin
   Response.Redirect("~/Login.usm")
   RETURN
ENDIF

lcId = Request.QueryString("id")

IF !this.oUserSecurity.Load(lcId)
   THIS.ErrorMsg("Couldn't delete User","Invalid User Id")
   RETURN
ENDIF

loUser = this.oUserSecurity.oUser

IF (!this.oUserSecurity.DeleteUser(lcId))
   THIS.ErrorMsg("Couldn't delete User",this.oUserSecurity.cErrorMsg)
ELSE
   this.StandardPage("User removed","User " + loUser.FullName + " has been deleted.","text/html",2,"~/usermanager/admin/userlist.usm")
ENDIF

ENDFUNC
*   DeleteUser

ENDDEFINE

