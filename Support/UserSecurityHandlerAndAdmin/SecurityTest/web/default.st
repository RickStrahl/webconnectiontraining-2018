﻿<% 	
    * VS Addin Comment: SourceFile="~\..\deploy\YOUR_PROCESS_CLASS.PRG"
    pcPageTitle = "content" 	
%>
<% Layout="~/views/_layoutpage.wcs" %>

<div class="container">
    
    <div class="page-header-text">
        <i class="fas fa-list"></i>
        Feature Samples
    </div>

    <p>
        Welcome to your new Web Connection application. This page is a temporary landing page so you can test your
        application's status.
        While developing it's a good idea to add common links
        to this page, so you have quick access to the pages you are
        working on.
    </p>


    <div class="panel panel-default" style="margin-top: 50px;">
        <div class="panel-heading">
            <h3 class="panel-title">Application Links</h3>
        </div>    

        <div class="list-group">
            <a href="SecuredContent.st" class="list-group-item">
                <i class="fas fa-bullhorn"></i> 
                Secured Content Page
            </a>
            <a href="admin/admin.aspx" class="list-group-item">
                <i class="fas fa-cog"></i> 
                Web Connection Administration
            </a>                
        </div>
    </div>    

</div> <!-- .container-->         

<!-- remove sections if you're not using them -->
<% section="headers" %>

<% endsection %>

<% section="scripts" %>

<% endsection %>