<% 	
	* VS Addin Comment: SourceFile="~\..\deploy\YOUR_PROCESS_CLASS.PRG"
	pcPageTitle = "SecuredContent" 	
%>
<% Layout="~/views/_layoutpage.wcs" %>

<div class="container">
    <div class="page-header-text">
        <i class="far fa-list-alt"></i> 
        SecuredContent
    </div>
 
    <markdown>
          ## Seeekrit Content

          Welcome you are authenticated and logged in.
    </markdown>

    You are: <b><%: Process.oUser.FullName %></b> at <b>level <%= Process.oUser.Level %></b>
    <hr/>
    
    Current Time is: <b><%= Time() %></b>
</div>            

<!-- remove sections if you're not using them -->
<% section="headers" %>

<% endsection %>

<% section="scripts" %>

<% endsection %>