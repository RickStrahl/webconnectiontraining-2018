# User Security Manager

This is a sample application that provides most of the components required to create logins in an application and provide basic user management. 

It provides:

* Login
* Password Recovery
* New Profile Creation
* Email Verification of new Users
* User Administration


This is a mostly self-contained example that can be plugged into other applications with minimal modifications and configuration.

## Run the Sample
To run this application in IIS:

To use IIS (make sure IIS is installed and works with Web Connection):

* `DO UserSecurityManager_serverconfig.prg` (requires Admin rights)
* Launch http://localhost/usersecuritymanager/

To use IIS Express:

* Open the Web Connection Menu (wcstart.prg)
* Set path to the Install folder's `Web/` folder
* Pick a port (8085)
* Launch http://localhost:8085

Go and play around with the app.

## Application Integreation
For this example I created a new Web Connection application and named it **SecurityTest**. So wherever you see `SecurityTest` in the name, just replace that with the name of your Project (ie. SecurityTestMain.prg becomes `YourAppMain.prg`).

### Copy Files and Folders
Copy the following files from the sample to your project.

#### Web

* Copy the `web\UserManager` folder
* Copy the `web\views\_Login.wcs` file

#### Code

* Copy `deploy\UserSecurityManagerProcess.prg`
* Copy `deploy\AppUserSecurity.prg`

### Add Code to SecurityTestMain.prg

#### Reference AppUserSecurity.prg
In the `YourApp::OnLoad()` method add the following:

```foxpro
PROTECTED FUNCTION OnLoad

*** Hook in UserSecurityManager
DO AppUserSecurity
ADDPROPERTY(this.oConfig,;
            "oUserSecurityManagerProcess",;
            CREATEOBJECT("UserSecurityManagerProcessConfig"))
...

ENDFUNC
```

This loads two configuration classes:

* AppUserSecurity UserSecurity sub-class
* UserSecurityManagerProcessConfig configuration class
* Adds the User Security class to the Configuration


#### Add USM Extension to your Process Handling
Add the following extension mapping to your `SecurityTestServer::Process()` method in the area where extensions are mapped:

```foxpro
CASE lcExtension == "USM"
	DO UserSecurityManagerProcess with THIS        
```

You'll also need a scriptmap for `.usm`. You can either manually add it.

#### Add the USM Scriptmap

In `YourApp.ini` add `usm` to the `ServerConfig::ScriptMaps` setting:

```ini
[ServerConfig]
Virtual=SecurityTest
ScriptMaps=wc,wcs,md,st,usm
IISPath=IIS://localhost/w3svc/1/root
```

If you're using IIS you can then just do (running under Admin)

```foxpro
DO SecurityTest_Config.prg
```

For IIS Express add the scriptmap manually to `web\web.config`:

```xml
<configuration
	...
	<system.webServer>
		...		
		<handlers>
			<add name=".wcs_wconnect-module" path="*.wcs" verb="*" type="Westwind.WebConnection.WebConnectionHandler,WebConnectionModule" preCondition="integratedMode"/>
			...
			
			<!-- *** add USM here *** -->
			<add name=".usm_wconnect-module" path="*.usm" verb="*" type="Westwind.WebConnection.WebConnectionHandler,WebConnectionModule" preCondition="integratedMode"/>
		</handlers>
	</system.webServer>
</configuration>	
```

### Customize the AppUserSecurity Class
`AppUserSecurity.prg` contains a subclass of [wwUserSecurity class](https://webconnection.west-wind.com/docs/_1p30ta7n1.htm) in which you can customize the behavior of your user security class.

You can specify whether new accounts have to be validated, and whether passwords are encrytped:

```foxpro
*************************************************************
DEFINE CLASS AppUserSecurity AS wwUserSecurity
*************************************************************
#IF .F.
*:Help Documentation
*:Topic:
Class AppUserSecurity

*:Description:
Create a custom User Security class that sets up that 
customizes user security. You should rename this class
for each application you create.

Then change the cAuthenticationUserSecurityClass in 
UserSecurityManagerProcess
*:ENDHELP
#ENDIF

*** overridden properties

*-- Alias for the user file 
calias = "AppUserSecurity"

*-- Filename for the user file.
cfilename = "AppUserSecurity"

*-- If .t. requires Email validation of new accounts
lRequireValidation = .T.

*-- if set encrypts the passwords
cPasswordEncryptionKey = "619fasd34ads"

ENDDEFINE
*EOC AppUserSecurity 
```

This file also contains the `UserSecurityManagerProcessConfig` which contains some operational parameters such as a company name and sender email address for sending validation and recovery emails. Set up both these classes.

```foxpro
*************************************************************
DEFINE CLASS UserSecurityManagerProcessConfig AS wwConfig
*************************************************************

cHTMLPagePath = "c:\WebConnectionProjects\UserSecurityManager\Web\"
cDATAPath = ""
cVirtualPath = "/UserSecurityManager/"

cEmailSenderName = "User Administration Manager"
cEmailSender = "admin@west-wind.com"
cEmailReplyTo = "noreply@west-wind.com"
cCompanyName = "ACME Products"
cCookieName = "_SecurityTest"

ENDDEFINE
```

### Configure your Process Class
In order for the processing to work we need to override the default Authentication behavior in your process class.

#### Configuration Settings Properties
First we need to tell the process class to use User Security Authentication and to use the AppUserSecurity class we configured earlier. Set the following properties at the top of the class:

```foxpro
cAuthenticationMode = "UserSecurity"  && `Basic` is default
cAuthenticationUserSecurityClass = "AppUserSecurity"
```

#### OnProcessInit()
Next we need to enable session state in your process class and enable authentication. The following example explicitly forces authentication to all requests except the home page and login/logout:

```foxpro
FUNCTION OnProcessInit
LOCAL lcScriptName, llIgnoreLoginRequest

*** Share the cookie with the UserSecurityManager cookie
THIS.InitSession(Server.oConfig.oUserSecurityManagerProcess.cCookieName,3600,.T.)

*** Force login to all requests EXCEPT the ones in the list
lcScriptName = LOWER(JUSTFNAME(Request.GetPhysicalPath()))
llIgnoreLoginRequest =  INLIST(lcScriptName,;
  "default","login","logout")
IF !THIS.Authenticate("any","",llIgnoreLoginRequest) 
   IF !llIgnoreLoginRequest
	  RETURN .F.
   ENDIF
ENDIF

...

```

#### Overriding Authentication Methods
Next we need to override the default behavior so that a User Identity `oUserIdentity` and `oUser` are **always available** in your code.

```FoxPro
************************************************************************
*  OnAuthenticated
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION OnAuthenticated()

IF TYPE("THIS.oUserSecurity") # "O"
   this.oUserSecurity = CREATEOBJECT(this.cAuthenticationUserSecurityClass)
   IF this.oUserSecurity.Load(this.cAuthenticatedUser)
	   this.oUser = this.oUsersecurity.oUser
   ELSE
       this.oUser = null
   ENDIF
ELSE
   this.oUser = this.oUsersecurity.oUser   
ENDIF

ENDFUNC
*   OnAuthenticated
```