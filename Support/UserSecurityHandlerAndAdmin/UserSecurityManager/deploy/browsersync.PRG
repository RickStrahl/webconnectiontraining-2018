************************************************************************
*  BrowserSync
****************************************
***  Function: Live Reload on save operations
***    Assume: Install Browser Sync requires Node/NPM:
***            npm install -g browser-sync
***      Pass:
***    Return:
************************************************************************
FUNCTION BrowserSync(lcUrl, lcPath, lcFiles)

IF EMPTY(lcUrl)
   lcUrl = "localhost/usersecuritymanager"
ENDIF
IF EMPTY(lcPath)
   lcPath = LOWER(FULLPATH("..\web"))
ENDIF
IF EMPTY(lcFiles)
   lcFiles = "**/*.usm, **/*.wcs, **/*.wc, css/*.css, scripts/*.js, **/*.htm*"
ENDIF

lcOldPath = CURDIR()
CD (lcPath)

lcBrowserSyncCommand = "browser-sync start " +;
                       "--proxy " + lcUrl + " " + ;
                       "--files '" + lcFiles + "'"
                       
RUN /n cmd /k &lcBrowserSyncCommand

*!*	? lcBrowserSyncCommand
*!*	_cliptext = lcBrowserSyncCommand

WAIT WINDOW "" TIMEOUT 1.5
CD (lcOldPath)

DO TimetrakkerMain

ENDFUNC
*   BrowserSync