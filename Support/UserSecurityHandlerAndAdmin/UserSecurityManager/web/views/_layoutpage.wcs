﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><%= IIF(vartype(pcPageTitle)="C",pcPageTitle,"") %></title>

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />

    <link rel="shortcut icon" href="~/favicon.ico" type="image/x-icon" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <link rel="apple-touch-icon" href="~/touch-icon.png" />

    <link rel="icon" href="~/touch-icon.png" />
    <meta name="msapplication-TileImage" content="~/touch-icon.png" />

    <link href="~/lib/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />    
    <link href="~/lib/fontawesome/css/all.min.css" rel="stylesheet" />
    <link href="~/css/application.css" rel="stylesheet" />
    

    <%= RenderSection("headers") %>
</head>
<body>
    <div class="banner">
        <!-- Slideout Menu Toggle - Hamburger menu -->
        <a class="slide-menu-toggle-open"
           title="More...">
            <i class="fa fa-bars"></i>
        </a>

        <!-- Icon and Company Logo -->
        <div class="title-bar">
            <a href="~/">
                <img class="title-bar-icon" src="~/images/icon.png" />
                <div style="float: left; margin: 4px 5px; line-height: 1.0">
                    <i style="color: #0092d0; font-size: 0.9em; font-weight: 600;">West Wind</i><br />
                    <i style="color: whitesmoke; font-size: 1.65em; font-weight: 600;">User Manager</i>
                </div>
            </a>
        </div>

        <!-- top right nav menu - .hidable for options that hide on small sizes -->
        <nav class="banner-menu-top float-right">
            <a href="#" class="hidable">
                <i class="fa fa-book"></i>
                Link
            </a>
            <a href="~/">
                <i class="fas fa-home"></i>
                Home
            </a>
            
            <%= RenderPartial("~/UserManager/LoginMenu_Partial.usm") %>
        </nav>
    </div>


    <div id="MainView">

        <%= RenderContent() %>

    </div> <!-- end #MainView -->


    <footer>
        <a href="http://www.west-wind.com/" class="float-right">
            <img src="~/images/WestwindText.png" />
        </a>
        <small>&copy; <%= Process.oConfig.cCompanyName %>,  <%= Year(Date()) %></small>
    </footer>



    <!-- slide in menu - Remove if you don't use it -->
    <nav class="slide-menu">
        <div style="padding: 10px 10px 10px 3px;">

            <a class="disabled">
                <i class="fas fa-home"></i>
                Main Menu
            </a>

            <a href="~/">
                <i class="fas fa-home"></i>
                Home
            </a>

            <% if (!Process.lIsAuthenticated) %>
            <a href="login.usm">
                <i class="fa fa-lock"></i>
                Sign in
            </a>
            <% else %>
            <a href="~/usermanager/profile.usm">
                <img src="<%= GravatarLink(Process.cAuthenticatedUser,23) %>" />
                <%: Process.oUser.FullName %>
            </a>
            <a href="~/logout.usm">
                <i class="fa fa-lock-open"></i>
                Sign out
            </a>
            <% endif %>

        </div>
    </nav>

    <script src="~/lib/jquery/dist/jquery.min.js"></script>

    <!--<script src="../scripts/ww.jquery.min.js"></script>-->

    <!--
        Add these only if you use Bootstrap dropdowns or modals
        And if you do: don't add here, only to content pages that actually need it
        <% section="scripts" %>
                <script src="~/lib/popper.js/dist/popper.min.js"></script>
                <script src="~/lib/bootstrap/dist/js/bootstrap.min.js"></script>
        <% endsection %>
    -->
    <script src="~/lib/popper.js/dist/popper.min.js"></script>
    <script src="~/lib/bootstrap/dist/js/bootstrap.min.js"></script>

    <script>
        // toggle menu, handle menu click and outside click to close
        $(document).on("click",
            ".slide-menu-toggle-open,.slide-menu-toggle-close," +
            ".slide-menu a, #SamplesLink,.slide-menu",
            function () {
                $(".slide-menu").toggleClass("active");
            });
    </script>

    <%= RenderSection("scripts") %>
</body>
</html>