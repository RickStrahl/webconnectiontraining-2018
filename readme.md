﻿# Web Connection Training 2018
### Materials, Notes and Support Materials

Thank you for attending the Training event at Southwest Fox. This repository contains all the Slides and documents for the sessions at the conference. This year's conference is a bit different than others in that it's mostly about code, so there are less support materials than usual.

But we also provide a host of features like Visual Studio Intellisense Snippets, FoxCode Intellisense snippets, and links to separate source code repositories for the two main sample applications we will build and talk about at the conference.

### Samples for Download

* [Full TimeTrakker Sample Application Code on Github](https://bitbucket.org/RickStrahl/swfox2018-timetrakker)  
  <img src="Assets/TimeTrakkerRepository.png" style="height: 120px;" />
  
* [Online Time Trakker Sample Application](http://swfoxtimetrakker.west-wind.com)  
<img src="Assets/TimeTrakkerOnlineSampleQr.png" style="height: 120px;" />

### Session Notes and Slides
The [Content](https://bitbucket.org/RickStrahl/webconnectiontraining-2018) folder contains all of the PowerPoint slides from the conference as well as any support whitepapers. If you're viewing the content online, you can browse to the documents and use the `Raw` view to see the actual document content displayed either in a browser viewer or downloaded and opened locally. Most papers are in Word document format and the slides use PowerPoint.


### Updates
I will be updating this example **before, during and after** the conference, so if you want to grab the files and locally use them it's probably a good idea to use Git to clone this repository, so you can later pull updates.

### Downloading Content from BitBucket
Cloning with Git is the best option as it allows you to keep updated with changes from the site.

But you can also download materials in any of the BitBucket repositories by clicking the Downloads button in the left section of the repo. You can switch branches and access specific tags to downloads specific versions/branches as needed. We'll be using branches for the code projects we work on during the conference.

### Git Clients
To help work with Git you might want to use a Git client other than the command line. The command line always works (once Git is installed), but personally I like to use a more interactive client:

* [TortoiseGit](https://tortoisegit.org/)  (for Windows Shell Integration in Explorer)
* [SourceTree](https://www.sourcetreeapp.com/)   (a nice and free Git UI) 
* [SmartGit](https://www.syntevo.com/smartgit/)  (commercial but really clean and very stable)

I use both mostly SmartGit plus TortoiseGit in combination. Tortoise is great when you're browsing files and also for quickly creating or cloning repos directly in Explorer. SmartGit (or SourceTree) are better for day to day operations when you need to see changes, look at history and manage Git interactions.